from flask import Blueprint, render_template, request, redirect, url_for, flash
from middlewares.middleware import check_token
from utils.validator import RegistrationClientForm, RegistrationClientsFileForm
from models.client import Client
from utils.db import db
import io
import csv

clients = Blueprint("clients", __name__)

# Vistas


@clients.route("/", methods=["GET", "POST"])
@check_token
def home():

    form = RegistrationClientForm()
    form_file = RegistrationClientsFileForm()

    if form.validate_on_submit() and form.submit.data:
        try:
            first_name = form.first_name.data
            last_name = form.last_name.data
            email = form.email.data
            phone = form.phone.data
            age = form.age.data
            country = form.country.data

            new_client = Client(first_name, last_name,
                                email, phone, int(age), country)

            db.session.add(new_client)
            db.session.commit()

            flash("Cliente guardado exitosamente", "success")

            return redirect(url_for("clients.home"))

        except:
            flash("Hubo un error al intentar guardar clientes, por favor revise informacion suministrada", "danger")

    if form_file.validate_on_submit() and form_file.submit_file.data:
        try:
            file = form_file.file.data
            stream = io.StringIO(
                file.stream.read().decode("UTF8"), newline=None)
            csv_input = csv.reader(stream)

            iter = 0
            for row in csv_input:
                if iter != 0:
                    first_name, last_name, email, phone, age, country = row
                    new_client = Client(
                        first_name, last_name, email, phone, int(age), country)

                    db.session.add(new_client)
                iter += 1

            print(csv_input)
            db.session.commit()

            flash("Clientes guardados exitosamente", "success")
            return redirect(url_for("clients.home"))

        except:
            flash("Hubo un error al intentar guardar clientes, por favor revise informacion suministrada", "danger")

    return render_template("index.html", form=form, form_file=form_file)


@clients.route("/clients")
@check_token
def list_clients():
    total_clients = Client.query.all()

    return render_template("clients.html", clients=total_clients)

# Consultas


@clients.route("/update_client/<id>", methods=["POST", "GET"])
@check_token
def modify_client(id):
    client_found = Client.query.get(id)
    if request.method == "POST":

        print("ID FROM POST")

        client_found.first_name = request.form["first_name"]
        client_found.last_name = request.form["last_name"]
        client_found.email = request.form["email"]
        client_found.phone = request.form["phone"]
        client_found.age = request.form["age"]
        client_found.country = request.form["country"]

        db.session.commit()

        flash("Cliente actualizado exitosamente")

        return redirect(url_for("clients.list_clients"))
    return render_template("update.html", client=client_found)


@clients.route("/delete_client/<id>")
@check_token
def remove_client(id):
    client_to_delete = Client.query.get(id)
    db.session.delete(client_to_delete)
    db.session.commit()

    flash("Cliente eliminado exitosamente")

    return redirect(url_for("clients.list_clients"))
