from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import StringField, PasswordField, EmailField, SubmitField, ValidationError, FileField
from wtforms.validators import DataRequired, Length
import re


class RegistrationClientForm(FlaskForm):
    first_name = StringField("Nombres",
                             validators=[DataRequired(), Length(min=3, max=100, message="Nombres debe tener entre 3 y 100 caracteres")])
    last_name = StringField("Apellidos",
                            validators=[DataRequired(), Length(min=3, max=100, message="Apellidos debe tener entre 3 y 100 caracteres")])
    email = EmailField("Correo",
                       validators=[DataRequired(), Length(min=5, max=100, message="Correo debe tener entre 5 y 100 caracteres")])
    phone = StringField("Telefono",
                        validators=[DataRequired()])
    age = StringField("Edad",
                      validators=[DataRequired()])
    country = StringField("Pais",
                          validators=[DataRequired(), Length(min=5, max=100, message="Correo debe tener entre 5 y 100 caracteres")])
    submit = SubmitField("Guardar")

    # Agregar validador para nombres y apellidos

    def validate_phone(form, field):
        if len(field.data) > 16 or len(field.data) < 5:
            raise ValidationError("Telefono debe ser entre 3 y 16 digitos")
        if not re.match("^[0-9]*$", field.data):
            raise ValidationError("Telefono no debe tener letras ni simbolos")

    def validate_age(form, field):
        if len(field.data) > 3:
            raise ValidationError("Edad debe ser de maximo 3 digitos")
        if not re.match("^[0-9]*$", field.data):
            raise ValidationError("Edad no debe tener letras ni simbolos")


class RegistrationClientsFileForm(FlaskForm):
    file = FileField("File", validators=[DataRequired(), FileAllowed(["csv"])])
    submit_file = SubmitField("Guardar")


class RegistrationUserForm(FlaskForm):
    username = StringField("Usuario",
                           validators=[DataRequired(), Length(min=3, max=100, message="Nombre de usuario debe tener entre 3 y 100 caracteres")])
    email = EmailField("Email",
                       validators=[DataRequired(), Length(min=5, max=100, message="Email debe tener entre 5 y 100 caracteres")])
    password = PasswordField("Contrasena",
                             validators=[DataRequired(), Length(min=5, max=50, message="Contrasena debe tener entre 5 y 50 caracteres")])
    submit = SubmitField("Registrarse")


class LoginUserForm(FlaskForm):
    email = EmailField("Email",
                       validators=[DataRequired(), Length(min=3, max=100, message="Email debe tener entre 3 y 100 caracteres")])
    password = PasswordField("Contrasena",
                             validators=[DataRequired(), Length(min=5, max=50, message="Contrasena debe tener entre 5 y 50 caracteres")])
    submit = SubmitField("Ingresar")
