import pyrebase


def generate_token(email, password):
    try:
        user = pyrebase.auth().sign_in_with_email_and_password(email, password)
        jwt = user['idToken']
        return jwt
    except:
        return None
