from flask import Blueprint, render_template, request, redirect, url_for, flash, make_response
from utils.generate_token import generate_token
from utils.validator import RegistrationUserForm, LoginUserForm
from models.user import User
from utils.db import db
import pyrebase
import firebase_admin
from firebase_admin import credentials
import json

users = Blueprint("users", __name__)

firebase = pyrebase.initialize_app(json.load(open("fbconfig.json")))
auth = firebase.auth()


@users.route("/signup", methods=["GET", "POST"])
def signup():

    if request.cookies.get('token'):
        return redirect(url_for("clients.home"))

    form = RegistrationUserForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        try:
            user = auth.create_user_with_email_and_password(email, password)
            user_info = auth.sign_in_with_email_and_password(email, password)
            token = user_info['idToken']

            flash("Usuario creado satisfactoriamente!", "success")

            print(token)
            resp = make_response(redirect("/"))
            resp.set_cookie('token', token)

            return resp

        except:
            print('Error creating user')
            return render_template("register.html", form=form)

    else:
        return render_template("register.html", form=form)


@users.route("/login", methods=["GET", "POST"])
def login():
    if request.cookies.get('token'):
        return redirect(url_for("clients.home"))

    form = LoginUserForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        try:
            user = auth.sign_in_with_email_and_password(email, password)

            token = user['idToken']

            resp = make_response(redirect("/"))
            resp.set_cookie('token', token)

            flash("Bienvenido!", "success")
            return resp
        except:
            flash("Ingreso fallido, credenciales incorrectas", "danger")
            return render_template("login.html", form=form)

    return render_template("login.html", form=form)


@users.route("/logout", methods=["GET", "POST"])
def delete_cookie():
    resp = make_response(redirect("/login"))
    resp.delete_cookie("token")

    return resp
