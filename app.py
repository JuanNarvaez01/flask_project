from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from routes.clients import clients
from routes.users import users
import firebase_admin
from firebase_admin import credentials

app = Flask(__name__)

app.secret_key = 'Secret_Key'
# app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:Juan123*@localhost/usersdb" MySQL
# app.config["SQLALCHEMY_DATABASE_URI"] = "mssql+pyodbc://JUANNARVAEZ\SQLEXPRESS/usersdb"
app.config["SQLALCHEMY_DATABASE_URI"] = "mssql+pyodbc://JUANNARVAEZ\SQLEXPRESS/usersdb?driver=ODBC+Driver+17+for+SQL+Server"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
SQLAlchemy(app)


app.register_blueprint(clients)
app.register_blueprint(users)
