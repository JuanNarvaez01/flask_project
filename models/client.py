from utils.db import db


class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    phone = db.Column(db.String(100))
    age = db.Column(db.Integer)
    country = db.Column(db.String(100))

    def __init__(self, first_name, last_name, email, phone, age, country):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = phone
        self.age = age
        self.country = country
