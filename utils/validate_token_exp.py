import jwt
import time


def verify_exp_token(token):
    payload = jwt.decode(token, options={"verify_signature": False})

    exp_time = int(payload["exp"])
    actual_time = int(time.time()) - int(time.timezone)

    if exp_time < actual_time:
        return False

    return True
