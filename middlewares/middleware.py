from enum import auto
from functools import wraps
from flask import request, redirect, url_for, make_response
from firebase_admin import auth
from routes.users import firebase
from utils.validate_token_exp import verify_exp_token

auth = firebase.auth()


def check_token(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        token = request.cookies.get('token')
        print(token)
        if not token:
            return redirect(url_for("users.login"))
        try:
            print(auth)
            # user = auth.verify_id_token(token)
            user = auth.get_account_info(token)

            is_valid_token = verify_exp_token(token)
            print(is_valid_token)

            if not is_valid_token:
                resp = make_response(redirect("/login"))
                resp.delete_cookie("token")

                return resp

            request.user = user

        except:
            resp = make_response(redirect("/login"))
            resp.delete_cookie("token")

            return resp
        return f(*args, **kwargs)
    return wrap
